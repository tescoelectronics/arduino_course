/*
 Ejemplo 1
*/
// Pin analógico asignado al Sensor de Temperatura LM35
const int LM35_PIN = 2;
const int Led_R =10;
const int Led_G =9;
const int Led_Y =8;

 
void setup() {
  // set the digital pin as output:
  pinMode(Led_R, OUTPUT);
  pinMode(Led_G, OUTPUT);
  pinMode(Led_Y, OUTPUT);
  //analogReference(INTERNAL); //tension volt internal reference
  Serial.begin(9600); //debug
}
 
void loop() {
  
  int input = analogRead(LM35_PIN);    // Obtengo el valor sensado por el LM35
  
  float mv  = (1100 / 1024.0) * input; // Convierto el valor leido a mV (ahora el "tope" es de 1100mV)
  float cel = mv / 10;                 // Convierto los mV leidos a grados celsius
   
  Serial.println(cel);//debug con valor grados
  if(40>cel){
    digitalWrite(Led_G, LOW);   
    delay(500);
    digitalWrite(Led_Y, LOW);   
    delay(500);
    digitalWrite(Led_R, LOW);
    delay(500);
  } else if(40<=cel && cel<=50){
    digitalWrite(Led_G, HIGH);
    delay(500);
    digitalWrite(Led_Y, LOW);   
    delay(500);
    digitalWrite(Led_R, LOW);
    delay(500);
  }else if(51<=cel && cel<=80){
    digitalWrite(Led_G, LOW);
    delay(500);
    digitalWrite(Led_Y, HIGH);   
    delay(500);
    digitalWrite(Led_R, LOW);
    delay(500);
  }else if(80<cel){
    digitalWrite(Led_G, LOW);
    delay(500);
    digitalWrite(Led_Y, LOW);   
    delay(500);
    digitalWrite(Led_R, HIGH);
    delay(500);
  }
}


